import java.util.Scanner;

class MenuShow {
	public static Scanner keyboard = new Scanner(System.in);
	static int choice;

	public int PrintMenu() {
		System.out.println("\n**** ระบบซ้อมรับปริญญา");
		System.out.println("**** 1. ลงทะเบียนบัณฑิตใหม่");
		System.out.println("**** 2. ซ้อมย่อย (2 ครั้ง)");
		System.out.println("**** 3. ซ้อมใหญ่");
		System.out.println("**** 4. ค้นหาผู้ขาดซ้อม");
		System.out.println("**** 5. รายงานผู้มีสิทธ์ิเข้ารับปริญญา");
		System.out.println("**** 6. จบการทา งาน-Exit");
		System.out.println("**** 7. รายงานผู้ที่ลงทะเบียน");
		System.out.println("**** หมายเหตุ  0: ขาดซ้อม  1: มาซ้อม ****");
		System.out.print("**** โปรดเลือกหัวข้อการทำงาน : ");

		choice = keyboard.nextInt();
		return (choice);
	}
};